import { createWebHistory, createRouter } from "vue-router";

/*======================================================
                    LOCAL COMPONENTS
======================================================*/
import Login from '../components/Login'
import Home from '../components/Home'
import About from '../components/About.vue'
import Contact from '../components/Contact.vue'
import Dashboard from '../components/Dashboard.vue'
import Product from '../components/Product.vue'
import Order from '../components/Order.vue'
import Customer from '../components/Customer.vue'
import Summary from '../components/Summary.vue'
import Footer from '../components/Footer.vue';
import NotFound from '../components/404.vue'


/*======================================================
                    DEFINE ROUTES
======================================================*/
const routes = [
  { path: '/login', component: Login },
  {
    path: '/',
    // Nested view
    children: [
      {
        path: '/dashboard/:totalSales?',
        // Nested named view
        components: {
          default: Dashboard,
          Summary,
        },
        name: 'Dashboard'
      },
      { path: '/product/:id?', component: Product },
      {
        path: '/order/:id/customer/:name/:age',
        component: Order,
        name: 'Order'
      },
      {
        path: '/customer/:id?', component: Customer, name: 'Customer'
      },
    ],
    // Named view
    components: {
      default: Home,
      Footer,
    },
    // Define same option for every component
    // props: true,
    // Define props options for each named view
    props: { default: true, Footer: false }
  },
  {
    path: '/very/long/path/name/to/about',
    component: About,
    alias: '/about',
    name: 'about',
  },
  { path: '/aboutUs', redirect: { path: '/about' } },
  // Dynamic redirect with function
  {
    path: '/aboutUs/:page', redirect: to => {
      return { path: '/about', query: { q: to.params.page } }
      // return { name: 'about', params: { p: to.params.page } }
    },
  },
  // Props: More usable
  //  (params): Only can pass data by router
  //` ( props): Can data  by rouaater & pass from parent to child component
  { path: '/contact/:phone?', name: 'Contact', component: Contact, props: true },
  { path: '/:pathMatch(.*)*', name: 'NotFound', component: NotFound },
];


const router = createRouter({
  history: createWebHistory(),
  routes,
});

/*======================================================
                    GLOBAL GUARDS
======================================================*/
// GLobal BEFORE hooks:
router.beforeEach((to, from) => {
  // console.log("Global (beforeEach)");

  const isCustomerLogin = false;
  // Disable navigate to login if already login
  if (to.fullPath === '/login' && isCustomerLogin) {
    // alert("You already login");
    return false;
  }
  return true;

  // Navigate to home page if customer already login
  // if (to.fullPath === '/login' && isCustomerLogin)
  //   next({ path: '/' }); //re-route
  // // next(false); //abort navigartion 
  // else next();
})

// Global beforeResolve 
router.beforeResolve((to, from, next) => {
  // console.log("Global (beforeResolve)");

  if (to.fullPath === '/login') {
    alert("Please login.");
  }
  next();
})

// GLobal AFTER hooks:
router.afterEach((to, from) => {
  // console.log(`Global (afterEach) - Just moved from '${from.path}' to '${to.path}'`)
})

export default router;